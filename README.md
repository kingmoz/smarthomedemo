# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This is a repo for the demo of HKSTP citychallenge competition.

It includes 3 programs:

1. Backend notification server (On master branch)

2. Mobile app (On app branch)

3. A raspberry pi program which trigger the notification (on RPi branch)

Demo video: [https://www.youtube.com/watch?v=Wt-d1xN6wH0](https://www.youtube.com/watch?v=Wt-d1xN6wH0)