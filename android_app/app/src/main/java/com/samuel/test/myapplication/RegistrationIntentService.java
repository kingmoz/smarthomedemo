package com.samuel.test.myapplication;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by king on 3/4/2016.
 */
public class RegistrationIntentService extends IntentService {
    private static final String TAG = "RegIntentService";
    private static final String SENT_TOKEN_TO_SERVER = "TOSERVER";

    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    public void onHandleIntent(Intent intent) {
        try {
            SharedPreferences sharedPreferences =
                    PreferenceManager.getDefaultSharedPreferences(this);
            InstanceID instanceID = InstanceID.getInstance(this);
            String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            Log.d("TAG", "token " + token);
            // Send token to server
            if (sendTokenToServer(token)) {
                sharedPreferences.edit().putBoolean(SENT_TOKEN_TO_SERVER, true).apply();
            }


        } catch (IOException e) {
            Log.e("TAG", "Registration error: " + e);
        }
    }

    private boolean sendTokenToServer(String token) {
        try {
            URL url = new URL("http://smarthome-cuhelloworld.rhcloud.com/register");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestMethod("POST");
            conn.connect();

            JSONObject jsonParam = new JSONObject();
            jsonParam.put("reg_id", token);
            OutputStreamWriter out = new OutputStreamWriter(conn.getOutputStream());
            out.write(jsonParam.toString());
            out.close();

            int response = conn.getResponseCode();
            if (response == 200) {
                Log.d(TAG, "Send token to server success.");
                return true;
            } else {
                Log.d(TAG, "Send token to server fail. Response code: " + response);
                return false;
            }

        } catch (Exception e) {
            Log.e(TAG, "Fail to send token to server: " + e);
            return false;
        }
    }
}