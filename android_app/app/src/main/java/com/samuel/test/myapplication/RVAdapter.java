package com.samuel.test.myapplication;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by king on 2/4/2016.
 */
public class RVAdapter extends RecyclerView.Adapter<RVAdapter.MessageViewHolder> {
    ArrayList<Message> mMessage;
    private static final String DATEFORMAT = "yyyy-MM-dd HH:mm";

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MessageViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        CardView cv;
        TextView message;
        TextView title;
        TextView dateTime;
        ImageView image;

        MessageViewHolder(View itemView) {
            super(itemView);
            cv = (CardView) itemView.findViewById(R.id.cv);
            title = (TextView) itemView.findViewById(R.id.title);
            message = (TextView) itemView.findViewById(R.id.message);
            dateTime = (TextView) itemView.findViewById(R.id.dateTime);
            image = (ImageView) itemView.findViewById(R.id.image);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public RVAdapter(ArrayList<Message> messageItem) {
        mMessage = messageItem;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MessageViewHolder onCreateViewHolder(ViewGroup parent,
                                                int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_view, parent, false);
        // set the view's size, margins, paddings and layout parameters
        MessageViewHolder vh = new MessageViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MessageViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        String title = mMessage.get(position).title;
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATEFORMAT);

        holder.title.setText(title);
        holder.message.setText(mMessage.get(position).message);
        holder.cv.findViewById(R.id.statusBar).setBackgroundResource(mMessage.get(position).color);
        holder.dateTime.setText(dateFormat.format(new Date()));
        holder.image.setImageResource(mMessage.get(position).image);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mMessage.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
