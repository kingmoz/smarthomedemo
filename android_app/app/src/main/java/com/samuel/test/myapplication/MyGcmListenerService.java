package com.samuel.test.myapplication;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;

/**
 * Created by king on 4/4/2016.
 */
public class MyGcmListenerService extends GcmListenerService {
    private static final String TAG = "GcmListenerService";
    private static final String GCM_RECEIVER = "GCM_RECEIVER";


    @Override
    public void onMessageReceived(String from, Bundle data) {
        String message = (String) data.get("message");
        Log.d(TAG, "From: " + from);
        Log.d(TAG, "Message: " + message);
        updateMainActivity(getApplicationContext(), message);
    }

    static void updateMainActivity(Context context, String message) {
        Intent intent = new Intent(GCM_RECEIVER);

        intent.putExtra("message", message);

        context.sendBroadcast(intent);
    }

}
