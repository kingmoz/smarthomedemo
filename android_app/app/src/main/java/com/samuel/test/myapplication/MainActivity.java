package com.samuel.test.myapplication;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static String TAG = "Main Activity";
    private static final String SENT_TOKEN_TO_SERVER = "TOSERVER";
    private static final String GCM_RECEIVER = "GCM_RECEIVER";

    private RecyclerView recyclerView;
    private ArrayList<Message> messageList = new ArrayList<>();
    private RVAdapter mAdapter;
    private boolean isReceiverRegistered;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        recyclerView = (RecyclerView) findViewById(android.R.id.list);

        recyclerView.setHasFixedSize(true);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        isReceiverRegistered = sharedPreferences.getBoolean(SENT_TOKEN_TO_SERVER, false);
        if (!isReceiverRegistered) {
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }

        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(llm);

        try {
            mAdapter = new RVAdapter(messageList);
            recyclerView.setAdapter(mAdapter);
        } catch (Exception e) {
            Log.e(TAG, "Main thread exception " + e);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        getApplicationContext().registerReceiver(mMessageReceiver, new IntentFilter(GCM_RECEIVER));
    }

    @Override
    protected void onPause() {
        super.onPause();
        getApplicationContext().unregisterReceiver(mMessageReceiver);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String message = intent.getStringExtra("message");
            Log.d(TAG, "BroadcastReceiver message: " + message);
            if (message.equals("Btn_1")) {
                messageList.add(0, new Message(getResources().getString(R.string.bathDetectionTitle),
                        getResources().getString(R.string.bathDetectionMessage), R.color.messageDanger, R.drawable.fall));
                recyclerView.scrollToPosition(0);
                mAdapter.notifyItemInserted(0);
            } else if (message.equals("Btn_2")) {
                messageList.add(0, new Message(getResources().getString(R.string.kitchenDetectionTitle),
                        getResources().getString(R.string.kitchenDetectionMessage), R.color.messageDanger, R.drawable.cook));
                recyclerView.scrollToPosition(0);
                mAdapter.notifyItemInserted(0);
            } else if (message.equals("Btn_3")) {
                messageList.add(0, new Message(getResources().getString(R.string.roomDetectionTitle),
                        getResources().getString(R.string.roomDetectionMessage), R.color.messageDanger, R.drawable.sleep));
                recyclerView.scrollToPosition(0);
                mAdapter.notifyItemInserted(0);
            } else if (message.equals("Btn_4")) {
                messageList.add(0, new Message(getResources().getString(R.string.elecDetectionTitle),
                        getResources().getString(R.string.elecDetectionMessage), R.color.messagePrimary, R.drawable.elec));
                recyclerView.scrollToPosition(0);
                mAdapter.notifyItemInserted(0);
            } else if (message.equals("Btn_5")) {
                messageList.add(0, new Message(getResources().getString(R.string.drugDetectionTitle),
                        getResources().getString(R.string.drugDetectionMessage), R.color.messagePrimary, R.drawable.drug));
                recyclerView.scrollToPosition(0);
                mAdapter.notifyItemInserted(0);
            } else if (message.equals("Btn_6")) {
                messageList.add(0, new Message(getResources().getString(R.string.leaveDetectionTitle),
                        getResources().getString(R.string.leaveDetectionMessage), R.color.messageDanger, R.drawable.fall));
                recyclerView.scrollToPosition(0);
                mAdapter.notifyItemInserted(0);
            } else if (message.equals("Btn_7")) {
                messageList.add(0, new Message(getResources().getString(R.string.backDetectionTitle),
                        getResources().getString(R.string.backDetectionMessage), R.color.messagePrimary, R.drawable.wake));
                recyclerView.scrollToPosition(0);
                mAdapter.notifyItemInserted(0);
            }

        }
    };
}