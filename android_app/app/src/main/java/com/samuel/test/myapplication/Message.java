package com.samuel.test.myapplication;

/**
 * Created by king on 2/4/2016.
 */
public class Message {
    String title;
    String message;
    int color;
    int image;
    Message(String title, String message, int color, int image) {
        this.title = title;
        this.message = message;
        this.color = color;
        this.image = image;
    }
}
